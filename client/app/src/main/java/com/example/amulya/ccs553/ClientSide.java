package com.example.amulya.ccs553;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;


public class ClientSide extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_side);

        Context context = this.getApplicationContext();
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        assert batteryStatus != null;
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;

        if (usbCharge) {
            Toast toast = Toast.makeText(context,"Charging via USB!", Toast.LENGTH_SHORT);
            toast.show();
            ASyncCallWS task = new ASyncCallWS();   //cannot do network operations on main thread!!!
            task.execute();
        } else {
            Toast toast = Toast.makeText(context,"NOT ON USB!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    /**
     * Asynchronous Call in Background to handle the socket connection to server
     */
    private class ASyncCallWS extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            sendString();
            return null;
        }
    }

    /**
     * Sends the string to the client specified at the ip address.
     * This should be done in background as Socket cannot be handled in main thread
     */
    private void sendString() {

        try {
          //  Toast t = Toast.makeText(this.getApplicationContext(), Inet4Address.getLocalHost().getHostAddress(), Toast.LENGTH_SHORT);
          //  t.show();
            //System.out.println("local host address :"+Inet4Address.getLocalHost().getHostAddress()); //bad!
            //get ip address of host using scanning
           // System.out.println("local host address :"+);

            String ipAddress = "192.168.233.128";//"172.31.212.69";//"172.31.134.175";

            InetAddress address = Inet4Address.getByName(ipAddress);
            Socket socket = new Socket(address,1149);

            String send = "please,get to end!";  //turn string to byte array
            byte[] byteArr = send.getBytes();
            OutputStream outputStream = socket.getOutputStream();
            System.out.println("Sending stuff!!");
            outputStream.write(byteArr, 0, byteArr.length);
            outputStream.flush();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_client_side, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
