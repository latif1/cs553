import java.io.*;
import java.net.*;
import java.nio.file.Files;

/**
 * This class facilitates the connection between server and client with key exchange and authentication.
 * Created by Amulya on 10/19/2015.
 */
public class Connect {

  public static void main(String[] args)  {

    int port = 1149;            //choose arbitrary port
    int byteStreamLength = 50;  //choose arbitrary file stream length
//    String outputFilePath = "C:\\Users\\Amulya\\Desktop\\trial.txt";
    String outputFilePath = "/home/zeus/Desktop/authorized_keys";
    //  int totalFileSize = 0;  //need this?
    int totalRead;

    //create socket = use socket to connect via USB between phone and server
    ServerSocket serverSocket;
    Socket socket;
    byte[] bytestream;
    InputStream inputStream;

    try {

      serverSocket = new ServerSocket(port);
      while (true) {//continuously waits for new connections
        System.out.println("Wait for connection...");
        socket = serverSocket.accept(); //got connection so now need to get file

        bytestream = new byte[byteStreamLength]; //byte array
        inputStream = socket.getInputStream();

      //  totalRead = inputStream.read(bytestream, 0, bytestream.length); //read from stream
        //   totalFileSize = totalRead;

        do {

          totalRead = inputStream.read(bytestream, 0, bytestream.length); //read from stream
          //     if (totalRead >-1) {
          //       totalFileSize += totalRead;
          //     }
        } while(totalRead >-1);


        File outputFile = new File(outputFilePath); //get the file we want to write to
        long spaceLeft = outputFile.getTotalSpace(); //how much space is there left in file?
        if (spaceLeft < bytestream.length) {
          System.out.println("OH NO Not enough space in file to add the incoming information!!!");
          socket.close();
          serverSocket.close();
        }

        PrintWriter printWriter= new PrintWriter(new BufferedWriter(new FileWriter(outputFilePath,true))); // append
        printWriter.append(new String(bytestream));
        printWriter.println();
        printWriter.println();
        printWriter.close();

        System.out.println("YAY DONE Writing!");
        //break;
      }
    } catch (IOException e) {

      System.out.println("OH NO!!! Exception");
    }


  }

}
